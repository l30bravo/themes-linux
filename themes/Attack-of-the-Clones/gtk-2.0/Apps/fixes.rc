# ==============================================================================
# OPEN OFFICE AND OTHER NON GTK FIXES
# ==============================================================================
style "no-gtk-base" 
{
fg[NORMAL]        	= @fg_color
fg[PRELIGHT]      	= @fg_color
fg[SELECTED]      	= @selected_fg_color
fg[ACTIVE]        	= @fg_color
fg[INSENSITIVE]   	= @fg_color

text[NORMAL]        	= @fg_color
text[PRELIGHT]      	= @fg_color
text[SELECTED]      	= @selected_fg_color
text[ACTIVE]        	= @fg_color
text[INSENSITIVE]   	= @fg_color

bg[NORMAL]        	= @bg_color
bg[PRELIGHT]      	= @bg_color
bg[SELECTED]      	= @selected_bg_color
bg[ACTIVE]        	= @bg_color
bg[INSENSITIVE]   	= @bg_color

base[NORMAL]        	= @bg_color
base[PRELIGHT]      	= @bg_color
base[SELECTED]      	= @selected_bg_color
base[ACTIVE]        	= @bg_color
base[INSENSITIVE]   	= @bg_color
}

style "no-gtk-light-base" = "no-gtk-base"
{

base[NORMAL]        	= lighter (@bg_color)
base[PRELIGHT]      	= lighter (@bg_color)
base[SELECTED]      	= @selected_bg_color
base[ACTIVE]        	= lighter (@bg_color)
base[INSENSITIVE]   	= lighter (@bg_color)
}

style "no-gtk-entry" = "no-gtk-light-base"
{
xthickness 		= 4
ythickness 		= 3
}

widget "GtkWindow"					style "no-gtk-light-base"
widget "GtkWindow.GtkFixed.*"				style "no-gtk-base"
#widget "GtkWindow.GtkFixed.GtkEntry" 			style:highest "no-gtk-entry"
#widget "GtkWindow.GtkFixed.GtkCombo*" 			style:highest "no-gtk-entry"
#widget "GtkWindow.GtkFixed.*Spin*" 			style:highest "no-gtk-entry"

# ==============================================================================
# GEDIT SPECIFIC SETTINGS
# ==============================================================================
style "gedit-frame"
{	
	engine "pixmap"
	{
 		image
		{
			function = SHADOW
                	file = ""
		}
	}
}

widget_class "*Gedit*.<GtkFrame>*"     			style "gedit-frame"
widget_class "*Gedit*.<GtkArrow>*"     			style:highest "default"


# ==============================================================================
# FIREFOX SPECIFIC SETTINGS
# ==============================================================================

style "ff-default" {

GtkCheckButton::indicator-size = 14

fg[NORMAL]        	= @fg_color
fg[PRELIGHT]      	= @fg_color
fg[SELECTED]      	= @selected_fg_color
fg[ACTIVE]        	= @fg_color
fg[INSENSITIVE]   	= darker (@bg_color)

bg[NORMAL]        	= @bg_color
bg[PRELIGHT]      	= shade (1.05, @selected_bg_color)
bg[SELECTED]	  	= @selected_bg_color
bg[INSENSITIVE]   	= @bg_color
bg[ACTIVE]        	= shade (1.04, @bg_color)
}

widget	"*MozillaGtkWidget*"	style	"ff-default"


# ==============================================================================
# EVOLUTION SPECIFIC SETTINGS
# ==============================================================================

style "evolution-default" {
#xthickness = 1

bg[NORMAL]        	= @bg_color
#bg[PRELIGHT]      	= shade (1.05, @selected_bg_color)
bg[SELECTED]	  	= @selected_bg_color
bg[INSENSITIVE]   	= @bg_color
bg[ACTIVE]        	= shade (1.04, @bg_color)

fg[NORMAL]        	= @fg_color
fg[PRELIGHT]      	= @fg_color
fg[SELECTED]      	= @selected_fg_color
fg[ACTIVE]        	= @fg_color
fg[INSENSITIVE]   	= darker (@bg_color)
}
style "evolution-combo" {

fg[NORMAL]        	= @fg_color
fg[PRELIGHT]      	= @fg_color
fg[SELECTED]      	= @selected_fg_color
fg[ACTIVE]        	= @fg_color
fg[INSENSITIVE]   	= darker (@bg_color)

text[NORMAL]        	= @fg_color
text[PRELIGHT]      	= @fg_color
text[SELECTED]      	= @selected_fg_color
text[ACTIVE]        	= @fg_color
text[INSENSITIVE]   	= darker (@bg_color)

bg[PRELIGHT]      	= shade (1.05, @selected_bg_color)
}
style "evolution-preview" 
{
fg[NORMAL]        	= @fg_color
fg[PRELIGHT]      	= @fg_color
fg[SELECTED]      	= @selected_fg_color
fg[ACTIVE]        	= @fg_color
fg[INSENSITIVE]   	= @fg_color

text[NORMAL]        	= @fg_color
text[PRELIGHT]      	= @fg_color
text[SELECTED]      	= @selected_fg_color
text[ACTIVE]        	= @fg_color
text[INSENSITIVE]   	= @fg_color

bg[NORMAL]        	= @bg_color
bg[PRELIGHT]      	= @bg_color
bg[SELECTED]      	= @selected_bg_color
bg[ACTIVE]        	= @bg_color
bg[INSENSITIVE]   	= @bg_color

base[NORMAL]        	= lighter (@bg_color)
base[PRELIGHT]      	= lighter (@bg_color)
base[SELECTED]      	= @selected_bg_color
base[ACTIVE]        	= lighter (@bg_color)
base[INSENSITIVE]   	= lighter (@bg_color)
}
style "evolution-thin" {xthickness = 1}

widget_class "*EShell*" 				style "evolution-default"
widget_class "*EShell*Combo*" 			style "evolution-combo"
widget_class "*EPaned*" 				style "evolution-thin"
widget_class "*EPreviewPane*" 			style "evolution-preview"

# ==============================================================================
# SOFTWARE CENTER SPECIFIC SETTINGS
# ==============================================================================
style "softwarecenter" {
fg[NORMAL]        	= @fg_color
fg[PRELIGHT]      	= @fg_color
fg[SELECTED]      	= @selected_fg_color
fg[ACTIVE]        	= @fg_color
fg[INSENSITIVE]   	= darker (@bg_color)

text[NORMAL]        	= @fg_color
text[PRELIGHT]      	= @fg_color
text[SELECTED]      	= @selected_fg_color
text[ACTIVE]        	= @fg_color
text[INSENSITIVE]   	= darker (@bg_color)

bg[NORMAL]        	= @bg_color
base[NORMAL]      	= @bg_color
}
style "softwarecenter-list" {
fg[NORMAL]        	= @bg_color

}

widget_class "*softwarecenter*softwarepane*" style "softwarecenter"
widget_class "*softwarecenter*softwarepane*appview*" style "softwarecenter-list"
widget_class "*softwarecenter*entry*" style "widest"
