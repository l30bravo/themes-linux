# ==============================================================================
# NAUTILUS SPECIFIC SETTINGS
# ==============================================================================

style "nautilus-sidebar"  = "default"
{

	text[NORMAL]	= @fg_color
	base[NORMAL]      = @bg_color
	font_name 	                  	= "Regular"

	GtkTreeView::odd_row_color		= @bg_color
	GtkTreeView::even_row_color		= @bg_color

      # these make the padding from left window edge a little more sane
	GtkTreeView::horizontal_separator	= 15
      	xthickness				= 0
	ythickness				= 0
}

style "nautilus-sidebar-other" = "nautilus-sidebar"
{

	xthickness = 0
	ythickness = 0
	base[NORMAL]      = @bg_color
	bg[NORMAL]	  = @bg_color

}

style "nautilus-breadcrumbs"
{
	xthickness = 0
	ythickness = 0

	fg[NORMAL]      = @text_color
	fg[PRELIGHT]    = @selected_fg_color
	fg[ACTIVE]      = shade (1.1, @text_color)
	fg[SELECTED]    = shade (1.1, @text_color)
	fg[INSENSITIVE] = @text_color


	GtkButton::inner-border				= { 10, 10, 3, 3}
	engine "pixmap"
	{	
		image
		{
			function        	= BOX
			shadow				= OUT
			recolorable     	= TRUE
			state				= PRELIGHT
			file				= "Images/prelight.png"
			border				= { 13, 13, 13, 13 }
			stretch         	= TRUE
		}		

		image
		{
			function        	= BOX
			recolorable     	= TRUE
			shadow				= IN
			state				= PRELIGHT
			file				= "Images/prelight.png"
			border				= { 13, 13, 13, 13 }
			stretch         	= TRUE
		}
	  
		image
		{
			function        	= BOX
			recolorable     	= TRUE
			state				= NORMAL
			file				= "Images/normal.png"
			border				= { 3, 13, 3, 3 }
			stretch         	= TRUE
		}
		
		image
		{
			function        	= BOX
			recolorable     	= TRUE
			state				= PRELIGHT
			file				= "Images/prelight.png"
			border				= { 13, 13, 13, 13 }
			stretch         	= TRUE
		}
	

		image
		{
			function        	= BOX
			recolorable     	= TRUE
			state				= ACTIVE
			file				= "Images/active.png"
			border				= { 13, 13, 3, 3 }
			stretch         	= TRUE
		}  

		image
		{
			function        	= BOX
			recolorable     	= TRUE
			state				= INSENSITIVE
			file				= "Images/normal.png"
			border				= { 3, 13, 3, 3 }
			stretch         	= TRUE
		} 

		image
		{
			function        	= BOX
			recolorable     	= TRUE
			state				= SELECTED
			file				= "Images/active.png"
			border				= { 13, 13, 3, 3 }
			stretch         	= TRUE
		}
	}
}



style "nautilus-left-slider"
{
	xthickness = 0
	ythickness = 0

#fg[INSENSITIVE] = @base_color	# INSENSITIVE ARROW

	engine "pixmap"
	{
		image {
			function 		= BOX
			file			= "Images/left_slider.png"
			border			= { 3, 3, 3, 3 }
			stretch			= TRUE
		}
	}

}



style "nautilus-right-slider"
{	
	xthickness = 0
	ythickness = 0





	engine "pixmap"
	{
		image {
			function 		= BOX
			file			= "Images/right_slider.png"
			border			= { 3, 3, 3, 3 }
			stretch			= TRUE
		}
	}
}
style "nautilus-mode-button"
{

	engine "pixmap"
	{	
		image
		{
			function        	= BOX
			recolorable     	= TRUE
			state				= PRELIGHT
			file				= "Images/mode_prelight.png"
			border				= { 3, 13, 3, 3 }
			stretch         	= TRUE
		}
	  
		image
		{
			function        	= BOX
			recolorable     	= TRUE
			state				= NORMAL
			file				= "Images/mode_normal.png"
			border				= { 3, 13, 3, 3 }
			stretch         	= TRUE
		}
	
		image
		{
			function        	= BOX
			recolorable     	= TRUE
			state				= ACTIVE
			file				= "Images/mode_pressed.png"
			border				= { 3, 13, 3, 3 }
			stretch         	= TRUE
		}  

		image
		{
			function        	= BOX
			recolorable     	= TRUE
			state				= INSENSITIVE
			file				= "Images/mode_normal.png"
			border				= { 3, 13, 3, 3 }
			stretch         	= TRUE
		} 

		image
		{
			function        	= BOX
			recolorable     	= TRUE
			state				= SELECTED
			file				= "Images/mode_pressed.png"
			border				= { 3, 13, 3, 3 }
			stretch         	= TRUE
		}
	}

}

style "nautilus-window-base"
{
xthickness = 2
ythickness =2

}
style "nautilus-window-bg"
{
	bg[NORMAL]	= @bg_color

}
style "nautilus-handle"
{
	GtkPaned::handle-size         = 0
	xthickness = 0
	ythickness = 0
#base[NORMAL]      = @sidebar_color
#	engine "pixmap"
#	{
#		image 
#		{
#		    function	= HANDLE
#		    recolorable	= TRUE
#		    file		= "Images/handle.png"
#		    stretch		= TRUE
#		    border 		= { 4, 4, 8, 8 }
#		}
#	}
}

style "arrows" = "default"
{
engine "pixmap" 
	{
		image
		{
			function		= ARROW
			recolorable		= TRUE
			overlay_file		= "Images/arrow-up.png"
			overlay_border		= { 0, 0, 0, 0}
			overlay_stretch		= FALSE
			arrow_direction		= UP
		}    
		image
		{
			function		= ARROW
			recolorable		= TRUE
			overlay_file		= "Images/arrow-down.png"
			overlay_border		= { 0, 0, 0, 0}
			overlay_stretch		= FALSE
			arrow_direction		= DOWN
		}    
		image
		{
			function		= ARROW
			recolorable		= TRUE
			overlay_file		= "Images/arrow-left.png"
			overlay_border		= { 0, 0, 0, 0}
			overlay_stretch		= FALSE
			arrow_direction		= LEFT
		}
		image
		{
			function		= ARROW
			recolorable		= TRUE
			overlay_file		= "Images/arrow-right.png"
			overlay_border		= { 0, 0, 0, 0}
			overlay_stretch		= FALSE
			arrow_direction		= RIGHT
		}
	}
}

#widget_class	"*Nautilus*Window*"		style "nautilus-window-base"
#widget_class	"*Nautilus*View*"		style "nautilus-window-bg"
widget		"*Nautilus*Splitter"		style "nautilus-handle"
#widget_class	"*Nautilus*Statusbar*"		style "nautilus-window-base"
#widget		"*Nautilus*GtkModeButton*"	style "notebook-thin" 
#widget_class	"*Nautilus*Toolbar*"		style "nautilus-window-bg" 

widget_class	"*Nautilus*Places*Sidebar*"	style "nautilus-sidebar"
widget_class "*Nautilus*EmblemSidebar*"		style "nautilus-sidebar-other"
widget_class "*Nautilus*Side*Tree*"		style "nautilus-sidebar-other"
widget_class "*Nautilus*Notes*"			style "nautilus-sidebar-other"
widget_class "*Nautilus*History*"		style "nautilus-sidebar-other"
widget_class "*Nautilus*Information*"		style "nautilus-sidebar-other"
widget	     "*GtkModeButton*"			style "nautilus-mode-button"
widget       "Nautilus*left_slider*" 		style "nautilus-left-slider" 
widget       "Nautilus*right_slider*" 		style "nautilus-right-slider" 
widget_class "*NautilusPathBar.*ToggleButton*" 	style "nautilus-breadcrumbs" 
widget_class "*Nautilus*<GtkArrow>"		style "arrows"
