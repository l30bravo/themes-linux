#! /bin/bash

#   + ----------------------------- +
#   |   Elegant GNOME               |
#   |   Author: Dmitriy Simbiriatin |
#   |   Email: <slpiv@mail.ru>      |
#   |   Version: 1.0                |
#   |   License: GPL 3              |
#   + ----------------------------- +

BACKUP_DIR="$HOME/.elegant_gnome_backup"
GDM_BACKUP="$BACKUP_DIR/gdm_backup"
BACKUP_NAME='elegant_gnome_bak'
GCONF_BACKUP='elegant_gnome_gconf'
TMP_DIR='/tmp/elegant-gnome'
CURRENT_DIR="`pwd`"
WALLPAPER_DIR='/usr/share/backgrounds'
WALLPAPER='gDIGE_by_Muscarr.png'
ICONS_DIR='/usr/share/icons'
THEMES_DIR='/usr/share/themes'
THEME_FILE='/usr/share/themes/Elegant-GTK/gtk-2.0/gtkrc'
ICONS='Elegant-AwOken'
GTK_THEME='Elegant-GTK'
CURSOR_THEME='Neutral++'
LOGO_NAME='start-here'
METACITY_THEME_SQUARED='Elegant-GTK-Squared'
METACITY_THEME_ROUNDED='Elegant-GTK-Rounded'
FONT_GENERAL="Droid Sans 9"
MONO_FONT="Droid Sans Mono 9"
TITLE_FONT="Droid Sans Bold 9"
FIREFOX_DIR="$HOME/.mozilla/firefox/*.default/chrome"
SMPLAYER_DIR="/usr/share/smplayer"
SMPLAYER_THEME="Elegant-GNOME"
GEDIT_SCHEME='oblivion'
NAUTSTR='include "nautilus.rc"'
SUSE_CHECK="`cat /proc/version | grep -o SUSE`"
DEBIAN_CHECK="`cat /proc/version | grep -o Debian | head -n 1`"
FEDORA_CHECK="`cat /proc/version | grep -o "Red Hat"`"

root_check() {
    if [ "$FEDORA_CHECK" = "Red Hat" ]; then
        if [ ! -f /usr/bin/beesu ]; then
            zenity --error --title="beesu" --text="Beesu is not installed on your system."
            exit 1
        fi
        ROOT='beesu'
    else
        if [ ! -f /usr/bin/gksu ]; then
            zenity --error --title="gksu" --text="Gksu is not installed on your system."
            exit 1
        fi
        ROOT='gksu'
    fi
}

suse_arch() {
    if [ -d /usr/lib64 ]; then
        ARCH='64'
    else
        ARCH='32'
    fi
}

reg_nautilus() {
(   
    root_check
    LINENUM="`awk "/$NAUTSTR/"' {print NR }' $THEME_FILE`"
    CURSTR="`awk '{ if (NR=='$LINENUM') print $0 }' $THEME_FILE`"
    FIRSTCRT="`echo ${CURSTR:0:1}`"
    
    if [ "$FIRSTCRT" = "#" ]; then
        zenity --error --title="Nautilus" --text="The theme is already set to regular Nautilus." 
        exit 1
    else
       zenity --info --title="Nautilus" --text="You'll have to enter your root password to install the Nautilus theme."
       $ROOT -- sed -i -e 's/include \"nautilus.rc\"/#include \"nautilus.rc\"/g' $THEME_FILE
       nautilus -q
    fi
) | 
zenity --width=400 --height=100 \
  	--progress --title="In progress..." \
	--text="Changing Nautilus theme..." \
	--window-icon=/usr/share/pixmaps/elegant-gnome.png \
	--percentage=0 --pulsate --auto-kill
}

elem_nautilus() {
(   
    root_check
    LINENUM="`awk "/$NAUTSTR/"' {print NR }' $THEME_FILE`"
    CURSTR="`awk '{ if (NR=='$LINENUM') print $0 }' $THEME_FILE`"
    FIRSTCRT="`echo ${CURSTR:0:1}`"
    
    if [ "$FIRSTCRT" = "#" ]; then
        zenity --info --title="Nautilus" --text="You'll have to enter your root password to install the Nautilus theme."
        $ROOT -- sed -i -e 's/#include \"nautilus.rc\"/include \"nautilus.rc\"/g' $THEME_FILE
        nautilus -q 
    else
        zenity --error --title="Nautilus" --text="The theme is already set to Nautilus Elementary."
        exit 1
    fi
) | 
zenity --width=400 --height=100 \
  	--progress --title="In progress..." \
	--text="Changing Nautilus theme..." \
	--window-icon=/usr/share/pixmaps/elegant-gnome.png \
	--percentage=0 --pulsate --auto-kill
}

installing_smplayer() {
(
    root_check
    if [ ! -d $SMPLAYER_DIR ]; then
        zenity --error --title="Smplayer" --text="Smplayer is not installed on your system."
        exit 1
    fi
    if [ ! -d $SMPLAYER_DIR/themes ]; then
        $ROOT mkdir $SMPLAYER_DIR/themes
    fi
    if [ -d $SMPLAYER_DIR/themes/$SMPLAYER_THEME ]; then
        zenity --error --title="Smplayer" --text="Smplayer theme is already installed on your system."
        exit 1
    fi
    zenity --info --title="Smplayer" --text="You'll have to enter your root password to install the Smplayer theme."
    echo "# Installing Smplayer theme..." && sleep 1
    $ROOT mv $ICONS_DIR/$ICONS/extra/smplayer/$SMPLAYER_THEME $SMPLAYER_DIR/themes 
) | 
zenity --width=400 --height=100 \
  	--progress --title="In progress..." \
	--text="Installing Smplayer theme..." \
	--window-icon=/usr/share/pixmaps/elegant-gnome.png \
	--percentage=0 --pulsate --auto-kill
}

removing_smplayer() {
(
    root_check
    if [ ! -d $SMPLAYER_DIR ]; then
        zenity --error --title="Smplayer" --text="Smplayer is not installed on your system. Nothing to remove."
        exit 1
    fi
    if [ ! -d $SMPLAYER_DIR/themes/$SMPLAYER_THEME ]; then
        zenity --error --title="Smplayer" --text="Smplayer theme is not installed on your system."
        exit 1
    fi
    zenity --info --title="Smplayer" --text="You'll have to enter your root password to remove the Smplayer theme."
    echo "# Removing Smplayer theme..." && sleep 1
    $ROOT mv $SMPLAYER_DIR/themes/$SMPLAYER_THEME $ICONS_DIR/$ICONS/extra/smplayer
) | 
zenity --width=400 --height=100 \
  	--progress --title="In progress..." \
	--text="Removing Smplayer theme..." \
	--window-icon=/usr/share/pixmaps/elegant-gnome.png \
	--percentage=0 --pulsate --auto-kill
}

installing_cpufreq() {
    (
    root_check
    if [ ! -d /usr/share/pixmaps/cpufreq-applet ]; then
        zenity --error --title="Cpu Freq Applet" --text="Cpu Freq Applet is not installed on your system."
        exit 1
    fi
    if [ -d /usr/share/pixmaps/cpufreq-appletbk ]; then
        zenity --error --title="Cpu Freq Applet" --text="Cpu Freq Applet theme is already installed on your system."
        exit 1
    fi
    zenity --info --title="Cpu Freq Applet" --text="You'll have to enter your root password to install the Cpu Freq Applet theme."
    echo "# Installing Cpufreq applet theme..." && sleep 1
    $ROOT mv /usr/share/pixmaps/cpufreq-applet /usr/share/pixmaps/cpufreq-appletbk
    $ROOT mv $ICONS_DIR/$ICONS/extra/cpufreq-applet /usr/share/pixmaps/cpufreq-applet
) | 
zenity --width=400 --height=100 \
  	--progress --title="In progress..." \
	--text="Installing Cpu Freq Applet theme..." \
	--window-icon=/usr/share/pixmaps/elegant-gnome.png \
	--percentage=0 --pulsate --auto-kill
    
}

removing_cpufreq() {
    (
    root_check
    if [ ! -d /usr/share/pixmaps/cpufreq-applet ]; then
        zenity --error --title="Cpu Freq Applet" --text="Cpu Freq Applet is not installed on your system."
        exit 1
    fi
    if [ ! -d /usr/share/pixmaps/cpufreq-appletbk ]; then
        zenity --error --title="Cpu Freq Applet" --text="Cpu Freq Applet theme is not installed on your system. Nothing to remove."
        exit 1
    fi
    zenity --info --title="Cpu Freq Applet" --text="You'll have to enter your root password to remove the Cpu Freq Applet theme."
    echo "# Restoring Cpu Freq Applet theme..." && sleep 1
    $ROOT mv /usr/share/pixmaps/cpufreq-applet $ICONS_DIR/$ICONS/extra/cpufreq-applet
    $ROOT mv /usr/share/pixmaps/cpufreq-appletbk /usr/share/pixmaps/cpufreq-applet
) | 
zenity --width=400 --height=100 \
  	--progress --title="In progress..." \
	--text="Removing Cpu Freq Applet theme..." \
	--window-icon=/usr/share/pixmaps/elegant-gnome.png \
	--percentage=0 --pulsate --auto-kill
}

installing_firefox() {
    if [ -d $FIREFOX_DIR ]; then
        echo "# Installing Firefox theme..." && sleep 1
        cp $THEMES_DIR/$GTK_THEME/userChrome.css $FIREFOX_DIR
        cp $THEMES_DIR/$GTK_THEME/userContent.css $FIREFOX_DIR
    else
        echo 'The firefox is not installed on this system'
    fi
}

removing_firefox() {
    if [ -d $FIREFOX_DIR ]; then
        echo "# Removing Firefox theme" && sleep 1
        rm $FIREFOX_DIR/userChrome.css
        rm $FIREFOX_DIR/userContent.css
    else
        echo 'The firefox is not installed on this system'
    fi
}

installing_pidgin() {
(
    root_check
    if [ ! -d /usr/share/pixmaps/pidgin ]; then
        zenity --error --title="Pidgin" --text="Pidgin is not installed on your system."
        exit 1
    fi
    if [ -d /usr/share/pixmaps/pidgin/statusbk ]; then
        zenity --error --title="Pidgin" --text="Pidgin theme is already installed on your system."
        exit 1
    fi
    zenity --info --title="Pidgin" --text="You'll have to enter your root password to install the Pidgin theme."
    echo "# Configuring pidgin's tray and status icons" && sleep 1
    $ROOT mv /usr/share/pixmaps/pidgin/status/ /usr/share/pixmaps/pidgin/statusbk
    $ROOT mv /usr/share/pixmaps/pidgin/tray/ /usr/share/pixmaps/pidgin/traybk
    $ROOT mv $ICONS_DIR/$ICONS/extra/pidgin/status/ /usr/share/pixmaps/pidgin/status
    $ROOT mv $ICONS_DIR/$ICONS/extra/pidgin/tray/ /usr/share/pixmaps/pidgin/tray
) | 
zenity --width=400 --height=100 \
  	--progress --title="In progress..." \
	--text="Installing Pidgin theme..." \
	--window-icon=/usr/share/pixmaps/elegant-gnome.png \
	--percentage=0 --pulsate --auto-kill
}

restoring_pidgin() {
(
    root_check
    if [ ! -d /usr/share/pixmaps/pidgin ]; then
        zenity --error --title="Pidgin" --text="Pidgin is not installed on your system."
        exit 1
    fi
    if [ ! -d /usr/share/pixmaps/pidgin/statusbk ]; then
        zenity --error --title="Pidgin" --text="Pidgin theme is not installed on your system. Nothing to remove."
        exit 1
    fi
    zenity --info --title="Pidgin" --text="You'll have to enter your root password to remove the Pidgin theme."
    echo "# Restoring the original pidgin's status and system tray icons..." && sleep 1
    $ROOT mv /usr/share/pixmaps/pidgin/status $ICONS_DIR/$ICONS/extra/pidgin/status
    $ROOT mv /usr/share/pixmaps/pidgin/tray $ICONS_DIR/$ICONS/extra/pidgin/tray
    $ROOT mv /usr/share/pixmaps/pidgin/statusbk/ /usr/share/pixmaps/pidgin/status
    $ROOT mv /usr/share/pixmaps/pidgin/traybk/ /usr/share/pixmaps/pidgin/tray
) | 
zenity --width=400 --height=100 \
  	--progress --title="In progress..." \
	--text="Removing Pidgin theme..." \
	--window-icon=/usr/share/pixmaps/elegant-gnome.png \
	--percentage=0 --pulsate --auto-kill
}

installing_emesene_suse() {
    suse_arch
    if [ "$ARCH" = "64" ]; then
        if [ ! -d /usr/lib64/emesene/themes/default ]; then
            zenity --error --title="Emesene" --text="Emesene is not installed on your system."
            exit 1
        fi
        if [ -d /usr/lib64/emesene/themes/default_bk ]; then
            zenity --error --title="Emesene" --text="Emesene theme is already installed on your system."
            exit 1
        fi
        zenity --info --title="Emesene" --text="You'll have to enter your root password to install the Emesene theme."
        echo "# Configuring emesene icon theme" && sleep 1
        $ROOT mv /usr/lib64/emesene/themes/default /usr/lib64/emesene/themes/default_bk
        $ROOT mv $ICONS_DIR/$ICONS/extra/emesene/default /usr/lib64/emesene/themes/default
    elif [ "$ARCH" = "32" ]; then
        if [ ! -d /usr/lib/emesene/themes/default ]; then
            zenity --error --title="Emesene" --text="Emesene is not installed on your system."
            exit 1
        fi
        if [ -d /usr/lib/emesene/themes/default_bk ]; then
            zenity --error --title="Emesene" --text="Emesene theme is already installed on your system."
            exit 1
        fi
        zenity --info --title="Emesene" --text="You'll have to enter your root password to install the Emesene theme."
        echo "# Configuring emesene icon theme" && sleep 1
        $ROOT mv /usr/lib/emesene/themes/default /usr/lib/emesene/themes/default_bk
        $ROOT mv $ICONS_DIR/$ICONS/extra/emesene/default /usr/lib/emesene/themes/default
    fi
}

installing_emesene_other() {
    if [ ! -d /usr/share/emesene/themes/default ]; then
        zenity --error --title="Emesene" --text="Emesene is not installed on your system."
        exit 1
    fi
    if [ -d /usr/share/emesene/themes/default_bk ]; then
        zenity --error --title="Emesene" --text="Emesene theme is already installed on your system."
        exit 1
    fi
    zenity --info --title="Emesene" --text="You'll have to enter your root password to install the Emesene theme."
    echo "# Configuring emesene icon theme" && sleep 1
    $ROOT mv /usr/share/emesene/themes/default /usr/share/emesene/themes/default_bk
    $ROOT mv $ICONS_DIR/$ICONS/extra/emesene/default /usr/share/emesene/themes/default
}

installing_emesene_main() {
(
    root_check
    if [ "$SUSE_CHECK" = "SUSE" ]; then
        installing_emesene_suse
    else
        installing_emesene_other
    fi
) | 
zenity --width=400 --height=100 \
  	--progress --title="In progress..." \
	--text="Installing Emesene theme..." \
	--window-icon=/usr/share/pixmaps/elegant-gnome.png \
	--percentage=0 --pulsate --auto-kill
}

restoring_emesene_suse() {
    suse_arch
    if [ "$ARCH" = "64" ]; then
        if [ ! -d /usr/lib64/emesene/themes/default ]; then
            zenity --error --title="Emesene" --text="Emesene is not installed on your system."
            exit 1
        fi
        if [ ! -d /usr/lib64/emesene/themes/default_bk ]; then
            zenity --error --title="Emesene" --text="Emesene theme is not installed on your system. Nothing to remove."
            exit 1
        fi
        zenity --info --title="Emesene" --text="You'll have to enter your root password to remove the Emesene theme."
        echo "# Restoring the original emesene icon theme..." && sleep 1
        $ROOT mv /usr/lib64/emesene/themes/default $ICONS_DIR/$ICONS/extra/emesene/default
        $ROOT mv /usr/lib64/emesene/themes/default_bk /usr/lib64/emesene/themes/default
    elif [ "$ARCH" = "32" ]; then
        if [ ! -d /usr/lib/emesene/themes/default ]; then
            zenity --error --title="Emesene" --text="Emesene is not installed on your system."
            exit 1
        fi
        if [ ! -d /usr/lib/emesene/themes/default_bk ]; then
            zenity --error --title="Emesene" --text="Emesene theme is not installed on your system. Nothing to remove."
            exit 1
        fi
        zenity --info --title="Emesene" --text="You'll have to enter your root password to remove the Emesene theme."
        echo "# Restoring the original emesene icon theme..." && sleep 1
        $ROOT mv /usr/lib/emesene/themes/default $ICONS_DIR/$ICONS/extra/emesene/default
        $ROOT mv /usr/lib/emesene/themes/default_bk /usr/lib/emesene/themes/default
    fi
}

restoring_emesene_other() {
    if [ ! -d /usr/share/emesene/themes/default ]; then
        zenity --error --title="Emesene" --text="Emesene is not installed on your system."
        exit 1
    fi
    if [ ! -d /usr/share/emesene/themes/default_bk ]; then
        zenity --error --title="Emesene" --text="Emesene theme is not installed on your system. Nothing to remove."
        exit 1
    fi
    zenity --info --title="Emesene" --text="You'll have to enter your root password to remove the Emesene theme."
    echo "# Restoring the original emesene icon theme..." && sleep 1
    $ROOT mv /usr/share/emesene/themes/default $ICONS_DIR/$ICONS/extra/emesene/default
    $ROOT mv /usr/share/emesene/themes/default_bk /usr/share/emesene/themes/default
}

restoring_emesene_main() {
(
    root_check
    if [ "$SUSE_CHECK" = "SUSE" ]; then
        restoring_emesene_suse
    else
        restoring_emesene_other
    fi
) | 
zenity --width=400 --height=100 \
  	--progress --title="In progress..." \
	--text="Removing Emesene theme..." \
	--window-icon=/usr/share/pixmaps/elegant-gnome.png \
	--percentage=0 --pulsate --auto-kill
}


installing_gdm_suse() {
    echo "# Backuping GDM2 settings..." && sleep 1
    for KEY in "/desktop/gnome/background/picture_filename"\
	    "/desktop/gnome/interface/gtk_theme"\
	    "/desktop/gnome/interface/icon_theme"\
	    "/apps/gdm/simple-greeter/logo_icon_name"
    do
        gconftool-2 --direct --config-source=xml::/etc/gconf/gconf.xml.vendor --get $KEY | tee -a $GDM_BACKUP
    done
    zenity --info --title="GDM2" --text="You'll have to enter your root password to install GDM2 theme."
    echo "# Installing GDM2 theme..." && sleep 1
    for KEY in "/desktop/gnome/background/picture_filename $WALLPAPER_DIR/$WALLPAPER"\
            "/desktop/gnome/interface/gtk_theme $GTK_THEME"\
            "/desktop/gnome/interface/icon_theme $ICONS"\
            "/apps/gdm/simple-greeter/logo_icon_name $LOGO_NAME"
    do
        $ROOT -- gconftool-2 --direct --config-source=xml::/etc/gconf/gconf.xml.vendor --set --type string $KEY
    done
}

installing_gdm_other() {
    zenity --info --title="GDM2" --text="You'll have to enter your root password to install GDM2 theme."
    echo "# Backuping GDM2 settings..." && sleep 1
    for KEY in "/desktop/gnome/background/picture_filename"\
	    "/desktop/gnome/interface/gtk_theme"\
	    "/desktop/gnome/interface/icon_theme"\
	    "/apps/gdm/simple-greeter/logo_icon_name"\
	    "/desktop/gnome/interface/font_name"
    do
        $ROOT -u gdm -- gconftool-2 --get $KEY | tee -a $GDM_BACKUP
    done
    echo "# Installing GDM2 theme..." && sleep 1
    for KEY in "/desktop/gnome/background/picture_filename $WALLPAPER_DIR/$WALLPAPER"\
            "/desktop/gnome/interface/gtk_theme $GTK_THEME"\
            "/desktop/gnome/interface/icon_theme $ICONS"\
            "/apps/gdm/simple-greeter/logo_icon_name $LOGO_NAME"\
            "/desktop/gnome/interface/font_name \"$FONT_GENERAL\""
    do
        $ROOT -u gdm -- gconftool-2 --set --type string $KEY
    done
}

installing_gdm_main() {
(
    root_check
    if [ ! -d $BACKUP_DIR ]; then
        zenity --error --title="GDM2" --text="Elegant GNOME Pack is not installed on your system."
        exit 1 
    fi
    if [ -f $GDM_BACKUP ]; then
        zenity --error --title="GDM2" --text="GDM2 theme is already installed on your system."
        exit 1
    fi
    if [ "$DEBIAN_CHECK" = "Debian" ]; then
        zenity --error --title="GDM2" --text="This function doesn't work in Debian."
        exit 1
    elif [ "$FEDORA_CHECK" = "Red Hat" ]; then
        zenity --error --title="GDM2" --text="This function doesn't work in Fedora."
        exit 1
    fi
    touch $GDM_BACKUP
    if [ "$SUSE_CHECK" = "SUSE" ]; then
        installing_gdm_suse
    else
        installing_gdm_other
    fi
) | 
zenity --width=400 --height=100 \
  	--progress --title="In progress..." \
	--text="Installing GDM2 theme..." \
	--window-icon=/usr/share/pixmaps/elegant-gnome.png \
	--percentage=0 --pulsate --auto-kill
}

restoring_gdm_suse() {
    echo "# Restoring GDM2 settings..." && sleep 1
    for KEY in "/desktop/gnome/background/picture_filename `awk 'NR == 1' $GDM_BACKUP`"\
            "/desktop/gnome/interface/gtk_theme `awk 'NR == 2' $GDM_BACKUP`"\
            "/desktop/gnome/interface/icon_theme `awk 'NR == 3' $GDM_BACKUP`"\
            "/apps/gdm/simple-greeter/logo_icon_name `awk 'NR == 4' $GDM_BACKUP`"
    do
        $ROOT -- gconftool-2 --direct --config-source=xml::/etc/gconf/gconf.xml.vendor --set --type string $KEY
    done
}

restoring_gdm_other() {
    echo "# Restoring GDM2 settings..." && sleep 1
    for KEY in "/desktop/gnome/background/picture_filename `awk 'NR == 1' $GDM_BACKUP`"\
            "/desktop/gnome/interface/gtk_theme `awk 'NR == 2' $GDM_BACKUP`"\
            "/desktop/gnome/interface/icon_theme `awk 'NR == 3' $GDM_BACKUP`"\
            "/apps/gdm/simple-greeter/logo_icon_name `awk 'NR == 4' $GDM_BACKUP`"\
            "/desktop/gnome/interface/font_name `awk 'NR == 5' $GDM_BACKUP`"
    do
        $ROOT -u gdm -- gconftool-2 --set --type string $KEY
    done
}

restoring_gdm_main() {
(
    root_check
    if [ ! -f $GDM_BACKUP ]; then
        zenity --error --title="GDM2" --text="GDM2 configuration backup doesn't exist."
        exit 1
    fi
    if [ "$SUSE_CHECK" = "SUSE" ]; then
        restoring_gdm_suse
    else
        restoring_gdm_other
    fi
    rm $GDM_BACKUP
) | 
zenity --width=400 --height=100 \
  	--progress --title="In progress..." \
	--text="Restoring GDM2 theme..." \
	--window-icon=/usr/share/pixmaps/elegant-gnome.png \
	--percentage=0 --pulsate --auto-kill
}

backuping_settings() {
    echo "# Backuping GNOME settings..." && sleep 1
    if [ -d $BACKUP_DIR ]; then
        rm -R $BACKUP_DIR
    fi
    mkdir $BACKUP_DIR
    if [ -d $TMP_DIR ]; then
        rm -R $TMP_DIR
    fi
    mkdir $TMP_DIR
    cd $HOME/
    tar -z -P --create --file $TMP_DIR/$GCONF_BACKUP.tar.gz .gconf
    gconftool-2 --dump / > $TMP_DIR/dump
    cd $TMP_DIR
    tar -z -P --create --file $BACKUP_DIR/$BACKUP_NAME.tar.gz *
    cd $CURRENT_DIR
    rm -R $TMP_DIR
}

restoring_backup() {
    echo "# Restoring GNOME Settings..." && sleep 1
    if [ -d $TMP_DIR ]; then
        rm -R $TMP_DIR
    fi
    mkdir $TMP_DIR
    tar --extract --directory=$TMP_DIR --file=$BACKUP_DIR/$BACKUP_NAME.tar.gz
    tar --extract --directory=$HOME --file=$TMP_DIR/$GCONF_BACKUP.tar.gz
    gconftool-2 --load $TMP_DIR/dump /
    rm -R $TMP_DIR
    rm -R $BACKUP_DIR
}

restoring_defaults() {
    echo "# Restoring settings to defaults..."
    for KEY in "/desktop/gnome/peripherals/keyboard/indicator"\
	    "/desktop/gnome/interface"\
	    "/desktop/gnome/peripherals/mouse"\
	    "/apps/nautilus/preferences"\
	    "/apps/metacity/general"\
 	    "/desktop/gnome/font_rendering"\
	    "/apps/gnome-terminal/profiles/Default"\
        "/apps/panel/toplevels/bottom_panel_screen0/background"\
 	    "/apps/panel/toplevels/top_panel_screen0/background"\
	    "/apps/gedit-2/preferences/editor/colors"\
	    "/desktop/gnome/background"
    do
        gconftool-2 --recursive-unset $KEY
    done
}

system_setting() {
    echo "# Enabling keyboard layout flags..." && sleep 1
    gconftool-2 --type boolean --set /desktop/gnome/peripherals/keyboard/indicator/showFlags true
    echo "# Setting icon theme..." && sleep 1
    gconftool-2 --set --type string /desktop/gnome/interface/icon_theme $ICONS
    echo "# Setting GTK+ theme ..." && sleep 1
    gconftool-2 --set --type string /desktop/gnome/interface/gtk_theme $GTK_THEME
    echo "# Setting cursor theme..." && sleep 1
    gconftool-2 --set --type string /desktop/gnome/peripherals/mouse/cursor_theme $CURSOR_THEME
    echo "# Configuring fonts..."
    gconftool-2 --set --type string /desktop/gnome/interface/document_font_name "$FONT_GENERAL"
    gconftool-2 --set --type string /desktop/gnome/interface/font_name "$FONT_GENERAL"  
    gconftool-2 --set --type string /desktop/gnome/interface/monospace_font_name "$MONO_FONT"
    gconftool-2 --set --type string /apps/nautilus/preferences/desktop_font "$FONT_GENERAL"
    gconftool-2 --set --type string /apps/metacity/general/titlebar_font "$TITLE_FONT"
    gconftool-2 --set --type string /desktop/gnome/font_rendering/antialiasing rgba
    gconftool-2 --set --type float /desktop/gnome/font_rendering/dpi 96
    gconftool-2 --set --type string /desktop/gnome/font_rendering/hinting slight
    gconftool-2 --set --type string /desktop/gnome/font_rendering/rgba_order rgb
    echo "# Configuring gnome-terminal..." && sleep 1
    gconftool-2 --set --type float /apps/gnome-terminal/profiles/Default/background_darkness 0.8
    gconftool-2 --set --type string /apps/gnome-terminal/profiles/Default/background_type transparent
    gconftool-2 --set --type string /apps/gnome-terminal/profiles/Default/scrollbar_position hidden
    gconftool-2 --set --type boolean /apps/gnome-terminal/profiles/Default/use_system_font true
    gconftool-2 --set --type boolean /apps/gnome-terminal/profiles/Default/use_theme_colors true
    echo "# Configuring nautilus..." && sleep 1
    gconftool-2 --set --type boolean /apps/nautilus/preferences/pathbar_like_breadcrumbs true
    gconftool-2 --set --type boolean /apps/nautilus/preferences/rgba_colormap false
    gconftool-2 --set --type string  /apps/nautilus/preferences/side_pane_view NautilusPlacesSidebar
    gconftool-2 --set --type boolean /apps/nautilus/preferences/sidebar_show_places_menu false
    echo "# Configuring metacity..." && sleep 1
    if [ "$METACITY_CHOISE" = "squared" ]; then
        gconftool-2 --set --type string /apps/metacity/general/theme $METACITY_THEME_SQUARED
    elif [ "$METACITY_CHOISE" = "rounded" ]; then
        gconftool-2 --set --type string /apps/metacity/general/theme $METACITY_THEME_ROUNDED
    fi
    echo "# Setting window buttons alignment" && sleep 1
    if [ "$ALIGNMENT" = "left" ]; then
        gconftool-2 --set --type string /apps/metacity/general/button_layout close,maximize,minimize:menu
    elif [ "$ALIGNMENT" = "right" ]; then
        gconftool-2 --set --type string /apps/metacity/general/button_layout menu:minimize,maximize,close
    fi
    echo "# Configuring gnome-panel..." && sleep 1
    gconftool-2 --set --type string /apps/panel/toplevels/bottom_panel_screen0/background/type image 
    gconftool-2 --set --type string /apps/panel/toplevels/bottom_panel_screen0/background/image $THEMES_DIR/$GTK_THEME/panel.png
    gconftool-2 --set --type boolean /apps/panel/toplevels/bottom_panel_screen0/background/stretch false
    gconftool-2 --set --type string /apps/panel/toplevels/top_panel_screen0/background/type image
    gconftool-2 --set --type string /apps/panel/toplevels/top_panel_screen0/background/image $THEMES_DIR/$GTK_THEME/panel.png
    gconftool-2 --set --type boolean /apps/panel/toplevels/top_panel_screen0/background/stretch false
    echo "# Setting wallpaper..." && sleep 1
    gconftool-2 --set --type string /desktop/gnome/background/picture_filename $WALLPAPER_DIR/$WALLPAPER
    echo "# Configuring gedit..." && sleep 1
    gconftool-2 --set --type string /apps/gedit-2/preferences/editor/colors/scheme $GEDIT_SCHEME
    echo "# Enabling icons in menus..." && sleep 1
    gconftool-2 --set --type boolean /desktop/gnome/interface/menus_have_icons true
}

configure_smplayer_menu() {
    smplayer=$(zenity --title "Elegant GNOME Pack" \
	    --text "Choose the action" \
	    --list --radiolist \
	    --window-icon=/usr/share/pixmaps/elegant-gnome.png \
	    --width 380 --height 200 \
	    --column "Entry" --column "Action" \
    True "Install Smplayer theme" \
    False "Remove Smplayer theme");
    
    case $smplayer in
    
    "Install Smplayer theme")  installing_smplayer
        ;;
    "Remove Smplayer theme")  removing_smplayer
        ;;
    esac
}

configure_pidgin_menu() {
    pidgin=$(zenity --title "Elegant GNOME Pack" \
	    --text "Choose the action" \
	    --list --radiolist \
	    --window-icon=/usr/share/pixmaps/elegant-gnome.png \
	    --width 380 --height 200 \
	    --column "Entry" --column "Action" \
    True "Install Pidgin theme" \
    False "Remove Pidgin theme");

    case $pidgin in

    "Install Pidgin theme")  installing_pidgin
        ;;
    "Remove Pidgin theme")  restoring_pidgin
        ;;
    esac
}

configure_emesene_menu() {
    emesene=$(zenity --title "Elegant GNOME Pack" \
	    --text "Choose the action" \
	    --list --radiolist \
	    --window-icon=/usr/share/pixmaps/elegant-gnome.png \
	    --width 380 --height 200 \
	    --column "Entry" --column "Action" \
    True "Install Emesene theme" \
    False "Remove Emesene theme");

    case $emesene in

    "Install Emesene theme")  installing_emesene_main
        ;;
    "Remove Emesene theme")  restoring_emesene_main
        ;;
    esac
}

metacity_theme() {
    metacity=$(zenity --title "Elegant GNOME Pack" \
	    --text "Choose the window border theme" \
	    --list --radiolist \
	    --window-icon=/usr/share/pixmaps/elegant-gnome.png \
	    --width 380 --height 200 \
	    --column "Entry" --column "Action" \
    True "Squared window border theme" \
    False "Rounded window border theme");
    
    case $metacity in

    "Squared window border theme")  METACITY_CHOISE='squared'
        ;;
    "Rounded window border theme")  METACITY_CHOISE='rounded'
        ;;
    esac
}

configure_gdm_menu() {
    gdm=$(zenity --title "Elegant GNOME Pack" \
	    --text "Choose the action" \
	    --list --radiolist \
	    --window-icon=/usr/share/pixmaps/elegant-gnome.png \
	    --width 380 --height 200 \
	    --column "Entry" --column "Action" \
    True "Install GDM2 theme" \
    False "Restore GDM2 settings");

    case $gdm in
    
    "Install GDM2 theme") installing_gdm_main
        ;;
    "Restore GDM2 settings") restoring_gdm_main
        ;;
    esac
}

configure_alignment_menu() {
    buttons=$(zenity --title "Elegant GNOME Pack" \
	    --text "Choose the window buttons alignment" \
	    --list --radiolist \
	    --window-icon=/usr/share/pixmaps/elegant-gnome.png \
	    --width 380 --height 200 \
	    --column "Entry" --column "Action" \
    False "Left side" \
    True "Right side");
    
    case $buttons in

    "Left side") ALIGNMENT='left'
        ;;
    "Right side") ALIGNMENT='right'
        ;;
    esac
}

configure_nautilus_menu() {
    nautilus=$(zenity --title "Elegant GNOME Pack" \
	    --text "Choose the Nautilus type" \
	    --list --radiolist \
	    --window-icon=/usr/share/pixmaps/elegant-gnome.png \
	    --width 380 --height 200 \
	    --column "Entry" --column "Action" \
    True "Regular Nautilus (comes with the GNOME by default)" \
    False "Nautilus Elementary");
    
    case $nautilus in

    "Regular Nautilus (comes with the GNOME by default)") reg_nautilus
        ;;
    "Nautilus Elementary") elem_nautilus
        ;;
    esac
}

configure_cpufreq_menu() {
    cpufreq=$(zenity --title "Elegant GNOME Pack" \
	    --text "Choose the action" \
	    --list --radiolist \
	    --window-icon=/usr/share/pixmaps/elegant-gnome.png \
	    --width 380 --height 200 \
	    --column "Entry" --column "Action" \
    True "Install Cpu Freq Applet theme" \
    False "Remove Cpu Freq Applet theme");
    
    case $cpufreq in

    "Install Cpu Freq Applet theme") installing_cpufreq
        ;;
    "Remove Cpu Freq Applet theme") removing_cpufreq
        ;;
    esac
}

help_dialog() {
(
echo -e "Configuration backup is in: ~/.elegant_gnome_backup.
Do NOT remove this directory, or you'll not be able to restore your previous settings\n
To install the Google Chrome theme:\n1. Download the theme at gnome-look.org and extract it.
2. Drag and drop the *.crx files into the Google Chrome window.\n 
Update instructions:\n1. Go to Applications -> Accessories -> Elegant GNOME
2. Choose the menu entry \"Reinstall the pack\"\n3. Restart the X server(Log out and then Log in)\n
Enabling the Smplayer theme:\n1. Go to Applications -> Accessories -> Elegant GNOME
2. Choose the menu entry \"Configure Smplayer theme\"\n3. Then choose the menu entry \"Install Smplayer theme\"
4. Run the Smplayer\n5. Go to Options -> Preferences, choose the Interface tab\n6. For the Icon set choose the Elegant-GNOME\n
Enabling the buddy list theme in Pidgin:\n1. Run Pidgin\n2. Go to Tools -> Preferences\n3. Choose the tab Themes
4. Select \"Elegant GNOME\" as a buddy list theme\n\nFirefox's checkboxes and radio buttons:
The solution that comes with the pack resolves almost all the problems with the inappropriate behavior
of the Firefox with the dark themes, but unfortunatelly it can't fix the style for the checkboxes and radio buttons.
So if you want try to resolve this problem completely, follow this link to get the instructions:
http://ubuntuforums.org/showthread.php?t=873486\n
To fix the wrong behavior of the OpenOffice with the dark themes, follow the next steps:
1. Start the OpenOffice.org\n2. Go to Tool -> Options\n3. Then OpenOffice.org -> Appearance
4. For the \"Document background\" choose - \"white\"\nFor the \"Text boundaries\" - \"Gray 80%\"
For the \"Application background\" - \"Gray 10%\"\nAnd for the \"Font color\" - \"black\""
) |
zenity --text-info --width 400 \
    --height=400 --title="Help" \
	--window-icon=/usr/share/pixmaps/elegant-gnome.png
}

about_dialog() {
(
echo -e "Elegant GNOME Pack v 1.0\nCustomize your GNOME desktop just in one click!
Copyright(c) 2010 Dmitriy Simbiriatin\n
License:\nElegant GNOME Pack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.\n
Elegant GNOME Pack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.\n
Credits:\n- alecive <alecive87@gmail.com>: for the great icon set
- Frank Kurian <frankkurian@gmail.com>: for the great help in testing 
  the pack on Ubuntu Linux
- Jorge Carrillo <jorge.car90@gmail.com>: for the help in creating
  the Google Chrome theme
- Corey Buckingham: for some fixes in the Elegant GTK theme
- Istvan Szabo: for the great help in testing the pack on OpenSuse
  and creating the fix for the Pidgin
- Everyone else who helped me in creating and testing this pack."
) |
zenity --text-info --width 550 \
    --height=400 --title="About" \
	--window-icon=/usr/share/pixmaps/elegant-gnome.png
}

installing_pack() {
(
    backuping_settings
    metacity_theme
    configure_alignment_menu
    system_setting
    installing_firefox
    zenity --info --title="Restart the X server" --text="Please restart the X server (log out and then log in) to complete the installation."
) | 
zenity --width=400 --height=100 \
    --progress --title="In progress..." \
	--text="Installing Elegant GNOME Pack..." \
	--window-icon=/usr/share/pixmaps/elegant-gnome.png \
	--percentage=0 --pulsate --auto-kill
}

removing_pack() {
(
    if [ -f $GDM_BACKUP ]; then
        zenity --error --title="GDM2" --text="Restore the GDM2 settings before removing the pack"
        exit 1
    else
        if [ -d $BACKUP_DIR ]; then
            restoring_backup
        else
            restoring_defaults
        fi
        removing_firefox
    fi
) | 
zenity --width=400 --height=100 \
  	--progress --title="In progress..." \
	--text="Removing Elegant GNOME Pack..." \
	--window-icon=/usr/share/pixmaps/elegant-gnome.png \
	--percentage=0 --pulsate --auto-kill
}

reinstall_pack() {
    if [ -f $GDM_BACKUP ]; then
        zenity --error --title="GDM2" --text="Restore the GDM2 settings before reinstalling the pack"
        exit 1
    else
        removing_pack
        installing_pack
    fi
}

action=$(zenity --title "Elegant GNOME Pack v 1.0" \
	--text "Choose the action" \
	--list --radiolist \
	--window-icon=/usr/share/pixmaps/elegant-gnome.png \
	--width 380 --height 380 \
	--column "Entry" --column "Action" \
False "Install the pack" \
False "Restore the previous settings" \
False "Reinstall the pack" \
False "Configure Nautilus" \
False "Configure GDM2 theme" \
False "Configure Cpu Freq Applet theme" \
False "Configure Pidgin theme" \
False "Configure Emesene theme" \
False "Configure Smplayer theme" \
True "Help" \
False "About");

case $action in

"Install the pack")  installing_pack
    ;;
"Restore the previous settings")  removing_pack
    ;;
"Reinstall the pack")  reinstall_pack
    ;;
"Configure Nautilus") configure_nautilus_menu
    ;;
"Configure GDM2 theme") configure_gdm_menu
    ;;
"Configure Cpu Freq Applet theme") configure_cpufreq_menu
    ;;
"Configure Pidgin theme") configure_pidgin_menu
    ;;
"Configure Emesene theme") configure_emesene_menu
    ;;
"Configure Smplayer theme") configure_smplayer_menu
    ;;
"Help") help_dialog
	;;
"About") about_dialog
    ;;
esac

exit 0
